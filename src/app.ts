import express from 'express';
import cors from 'cors';
import config from './configs/config';
import { connect } from './configs/database.config';
import user from './routes/user.route';

const app = express();

connect()
  .then(() => {
    console.log('database connected');
  })
  .catch((err) => {
    console.log('database connection failed');
    console.log(err);
  });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/users', user);

app.listen(config.port, () => {
  console.log(`server already running on port ${config.port}`);
  console.log(`http://localhost:${config.port}`);
});
