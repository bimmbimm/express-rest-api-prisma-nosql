import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const connect = async () => {
  try {
    await prisma.$connect();
  } catch (error) {
    await prisma.$disconnect();
  }
};

export { connect, prisma };
