import { Request, Response } from 'express';
import { prisma } from '../configs/database.config';

const findMany = async (req: Request, res: Response) => {
  try {
    const users = await prisma.user.findMany();
    res.status(200).json({
      code: 200,
      message: 'success',
      data: users,
      success: true,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      code: 500,
      message: 'internal server error',
      success: false,
    });
  }
};

const findOne = async (req: Request, res: Response) => {
  try {
    const user = await prisma.user.findUnique({
      where: { id: req.params.id },
    });
    res.status(200).json({
      code: 200,
      message: 'success',
      data: user,
      success: true,
    });
  } catch (err) {
    res.status(500).json({
      code: 500,
      message: 'internal server error',
      success: false,
    });
  }
};

const createOne = async (req: Request, res: Response) => {
  try {
    const user = await prisma.user.create({
      data: { ...req.body },
    });
    res.status(200).json({
      code: 200,
      message: 'success',
      data: user,
      success: true,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      code: 500,
      message: 'internal server error',
      success: false,
    });
  }
};

const updateOne = async (req: Request, res: Response) => {
  try {
    const user = await prisma.user.update({
      where: { id: req.params.id },
      data: { ...req.body },
    });
    res.status(200).json({
      code: 200,
      message: 'success',
      data: user,
      success: true,
    });
  } catch (err) {
    res.status(500).json({
      code: 500,
      message: 'internal server error',
      success: false,
    });
  }
};

const removeOne = async (req: Request, res: Response) => {
  try {
    const user = await prisma.user.delete({
      where: { id: req.params.id },
    });
    res.status(200).json({
      code: 200,
      message: 'success',
      data: user,
      success: true,
    });
  } catch (err) {
    res.status(500).json({
      code: 500,
      message: 'internal server error',
      success: false,
    });
  }
};

export { findMany, findOne, createOne, updateOne, removeOne };
